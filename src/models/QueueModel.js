import { types } from "mobx-state-tree";
import VisitorModel from "./VisitorModel";

const QueueModel = types.model("queue", {
  id: types.number,
  name: types.string,
  description: types.string,
  visitors: types.optional(types.array(VisitorModel), [])
})
.views(self => ({
  get avgWaitTime() {
    return 13;
  },
  get maxWaitTime() {
    return 27;
  }
}));

export default QueueModel;

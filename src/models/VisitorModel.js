import { types } from "mobx-state-tree";

const VisitorModel = types.model("visitor", {
  id: types.number,
  fullName: types.string
});

export default VisitorModel;

/* eslint-disable */
import React from "react";
import { create } from "jss";
import preset from "jss-preset-default";

// Must run before any MUI components are importaed
import { ThemeProvider, install } from "@material-ui/styles";

install();

// eslint-ignore
import { MuiThemeProvider } from "@material-ui/core/styles";
import { MuiPickersUtilsProvider } from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import JssProvider from "react-jss/lib/JssProvider";

//import orange from "@material-ui/core/colors/deepOrange";
import red from "@material-ui/core/colors/red";
import { createMuiTheme } from "@material-ui/core/styles";
import { BrowserRouter } from "react-router-dom";
import Layout from "./routes/Layout";

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      main: "#00695C",
      contrastText: "#FFF"
    },
    secondary: {
      main: "#6a1b9a",
      contrastText: "#FFF"
    },
    error: {
      ...red
    }
  }
});

const jss = create(preset());

const App = () => {
  return (
    <JssProvider jss={jss}>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          <MuiPickersUtilsProvider utils={DateFnsUtils}>
            <BrowserRouter>
              <Layout />
            </BrowserRouter>
          </MuiPickersUtilsProvider>
        </ThemeProvider>
      </MuiThemeProvider>
    </JssProvider>
  );
};

export default App;

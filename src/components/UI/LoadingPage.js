import * as React from "react";
import { CircularProgress } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    marginBottom: "1rem",
    fontSize: "1.2rem"
  }
}));

const LoadingPage = () => {
  const c = useStyles();
  return (
    <div className={c.root}>
      <div className={c.text}>Loading</div>
      <CircularProgress size={50} />
    </div>
  );
};

export default LoadingPage;

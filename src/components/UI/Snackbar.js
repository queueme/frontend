import CloseIcon from "@material-ui/icons/Close";
import { IconButton, Snackbar } from "@material-ui/core";
import { observer } from "mobx-react-lite";
import * as React from "react";
import NotificationContext from "../../contexts/NotificationContext";

const AppSnackbar = () => {
  const [open, setOpen] = React.useState(false);
  const [queue, setQueue] = React.useState([]);
  const [notification, setNotification] = React.useState(undefined);
  const store = React.useContext(NotificationContext);

  const scheduleNotification = next => {
    queue.push(next);
    setQueue(queue);
    if (open) {
      setOpen(false);
    } else {
      handleNext();
    }
  };

  const handleNext = () => {
    if (queue.length) {
      const notification = queue.shift();
      setQueue(queue);
      setNotification(notification);
      setOpen(true);
    }
  };

  const handleAutoHide = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleClose = event => {
    setOpen(false);
  };

  React.useEffect(
    () => {
      if (store.notifications.length) {
        const next = store.notifications[store.notifications.length - 1];
        scheduleNotification(next);
      }
    },
    [store.notifications.length]
  );

  const n = notification;

  return (
    <Snackbar
      open={open}
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "center"
      }}
      autoHideDuration={n && n.duration}
      onClose={handleAutoHide}
      ContentProps={{
        "aria-describedby": "message-id"
      }}
      message={<span>{n && n.message}</span>}
      onExited={handleNext}
      action={[
        <IconButton
          key="close"
          aria-label="Close"
          color="inherit"
          onClick={handleClose}
        >
          <CloseIcon />
        </IconButton>
      ]}
    />
  );
};

export default observer(AppSnackbar);

let token = "";
const ApiUrl = process.env.REACT_APP_API_URL || "https://queueme-backend.queueme.tech";

export const setToken = newToken => (token = newToken);

const post = (path, body) =>
  fetch(`${ApiUrl}/v1${path}`, {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    headers: {
      Authorization: token,
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify(body)
  })
    .then(res => res.json())
    .then(res => {
      if (res.error) {
        throw res.error;
      }
      return res;
    });

const get = path =>
  fetch(`${ApiUrl}/v1${path}`, {
    method: "GET",
    mode: "cors",
    cache: "no-cache",
    headers: {
      Authorization: token
    }
  })
    .then(res => res.json())
    .then(res => {
      if (res.error) {
        throw res.error;
      }
      return res;
    });

const sendDelete = (path, body) =>
  fetch(`${ApiUrl}/v1${path}`, {
    method: "DELETE",
    mode: "cors",
    cache: "no-cache",
    headers: {
      Authorization: token,
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: JSON.stringify(body)
  })
    .then(res => res.json())
    .then(res => {
      if (res.error) {
        throw res.error;
      }
      return res;
    });

export const login = (email, password) =>
  post("/users/login", { email, password });

export const register = (name, email, password) =>
  post("/users", { fullName: name, email, password });

export const createVenue = (name, description, address) =>
  post("/venues", { name, description, address });

export const getVenues = () => get("/venues");

export const createQueue = (venueId, name, description) =>
  post("/queues", { venueId, name, description });

export const deleteVenue = venueId => sendDelete("/venues", { id: venueId });
export const deleteQueue = queueId => sendDelete("/queues", { id: queueId });

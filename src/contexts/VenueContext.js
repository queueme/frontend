import React from "react";
import { types, flow } from "mobx-state-tree";
import {
  createVenue as doCreateVenue,
  getVenues,
  createQueue as doCreateQueue,
  deleteVenue as doDeleteVenue,
  deleteQueue as doDeleteQueue
} from "../lib/api";
import QueueModel from "../models/QueueModel";

const VenueModel = types
  .model("venue", {
    id: types.number,
    name: types.string,
    description: types.string,
    queues: types.optional(types.array(QueueModel), []),
    loading: false,
    address: types.optional(types.string, "")
  })
  .views(self => ({
    get visitors() {
      return 25;
    },
    get avgWaitTime() {
      return 8;
    },
    get maxWaitTime() {
      return 21;
    }
  }))
  .actions(self => {
    const deleteQueue = flow(function*(queue) {
      try {
        self.loading = true;

        const result = yield doDeleteQueue(queue.id);

        self.loading = false;

        return result;
      } catch (error) {
        return handleCatch(error);
      }
    });
    function removeQueue(queue) {
      self.queues.remove(queue);
    }
    const createQueue = flow(function*(name, desc) {
      try {
        self.loading = true;

        const result = yield doCreateQueue(self.id, name, desc);

        self.queues.push(result.queue);

        self.loading = false;

        return result;
      } catch (error) {
        return handleCatch(error);
      }
    });
    function handleCatch(error) {
      console.log(error);
      self.loading = false;
      if (typeof error === "string") {
        return { success: false, message: error };
      }
      return { success: false, message: "An unknown error has occurred" };
    }
    return { createQueue, deleteQueue, removeQueue };
  });

const VenueStoreModel = types
  .model("venueStore", {
    list: types.optional(types.array(VenueModel), []),
    loading: false
  })
  .actions(self => {
    const deleteVenue = flow(function*(venue) {
      try {
        self.loading = true;

        const result = yield doDeleteVenue(venue.id);

        self.loading = false;

        return result;
      } catch (error) {
        return handleCatch(error);
      }
    });
    function removeVenue(venue) {
      self.list.remove(venue);
    }
    const createVenue = flow(function*(name, description, address) {
      try {
        self.loading = true;

        const result = yield doCreateVenue(name, description, address);

        self.list.push(result.venue);

        self.loading = false;

        return result;
      } catch (error) {
        return handleCatch(error);
      }
    });

    const loadVenues = flow(function*() {
      try {
        self.loading = true;

        const result = yield getVenues();

        self.list.replace(
          result.venues.map(v => VenueModel.create({ ...v, queues: v.Queues }))
        );
        self.loading = false;

        return result;
      } catch (error) {
        return handleCatch(error);
      }
    });

    function handleCatch(error) {
      console.log(error);
      self.loading = false;
      if (typeof error === "string") {
        return { success: false, message: error };
      }
      return { success: false, message: "An unknown error has occurred" };
    }

    return { createVenue, loadVenues, removeVenue, deleteVenue };
  });

const VenueContext = React.createContext(VenueStoreModel.create());

export default VenueContext;

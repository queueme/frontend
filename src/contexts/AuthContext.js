import React from "react";
import { types, flow } from "mobx-state-tree";
import { setToken, login as doLogin, register as doRegister } from "../lib/api";
import jwtDecode from "jwt-decode";

const AuthModel = types
  .model("auth", {
    authed: false,
    loading: false,
    userId: types.optional(types.integer, 0),
    fullName: types.optional(types.string, ""),
    email: types.optional(types.string, "")
  })
  .views(self => ({
    get initials() {
      return self.fullName
        .split(" ")
        .map(el => (el ? el[0].toUpperCase() : ""))
        .slice(0, 2);
    }
  }))
  .actions(self => {
    function afterCreate() {
      // Validate session storage
      const token = sessionStorage.getItem("token");
      if (token) {
        setToken(token); // Set API token
        decodeToken(token); // Store in session, parse, set auth state
      }
    }

    const login = flow(function*(email, password) {
      try {
        self.loading = true;

        const result = yield doLogin(email, password);

        if (result.success) {
          decodeToken(result.token);

          return result;
        } else {
          throw result;
        }
      } catch (error) {
        return handleCatch(error);
      }
    });

    const register = flow(function*(name, email, password) {
      try {
        self.loading = true;

        const result = yield doRegister(name, email, password);

        if (result.success) {
          decodeToken(result.token);

          return result;
        } else {
          throw result;
        }
      } catch (error) {
        return handleCatch(error);
      }
    });

    function logout() {
      sessionStorage.removeItem("token");
      self.authed = false;
    }

    function handleCatch(error) {
      console.log(error);
      self.loading = false;
      if (typeof error === "string") {
        return { success: false, message: error };
      }
      return { success: false, message: "An unknown error has occurred" };
    }

    function decodeToken(token) {
      sessionStorage.setItem("token", token);
      try {
        const jwt = jwtDecode(token.split(" ")[1]);

        if (jwt.exp < new Date().getTime() / 1000) {
          logout();
          return;
        }

        self.fullName = jwt.fullName;
        self.email = jwt.email;
        self.userId = jwt.user_id;
        self.authed = true;
      } catch (error) {
        self.authed = false;
        console.log(error);
      }
    }

    return { afterCreate, login, register, logout };
  });

const AuthContext = React.createContext(AuthModel.create());

export default AuthContext;

import React from "react";
import { types } from "mobx-state-tree";

const NotificationModel = types
  .model("notification", {
    message: types.string,
    duration: types.number,
    open: true
  })
  .actions(self => ({
    close() {
      self.open = false;
    }
  }));

const NotificationStoreModel = types
  .model("notificationStore", {
    notifications: types.optional(types.array(NotificationModel), [])
  })
  .actions(self => ({
    sendNotification(message, duration = 2000) {
      self.notifications.push({ message, duration });
    }
  }));

const NotificationContext = React.createContext(
  NotificationStoreModel.create()
);

export default NotificationContext;

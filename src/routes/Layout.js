import React from "react";
import { Route, Switch } from "react-router-dom";
import Landing from "./Landing/Landing";
import Dashboard from "./Dashboard/Dashboard";

const Layout = () => (
  <Switch>
    <Route exact path="/" component={Landing} />
    <Route path="/dashboard" component={Dashboard} />
    <Route component={() => <div>Not Found</div>} />
  </Switch>
);

export default Layout;

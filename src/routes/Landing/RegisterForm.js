import React from "react";
import { makeStyles } from "@material-ui/styles";
import { TextField, Button } from "@material-ui/core";
import LoginContext from "../../contexts/LoginContext";
import blue from "@material-ui/core/colors/blue";
import { AuthContext, NotificationContext } from "../../contexts";
import { observer } from "mobx-react-lite";

const useStyles = makeStyles(theme => ({
  root: {},
  topText: {
    textAlign: "center"
  },
  textField: {
    display: "block !important"
  },
  createButton: {
    marginTop: "1rem",
    display: "flex",
    justifyContent: "center"
  },
  loginText: {
    display: "inline-block",
    cursor: "pointer",
    color: blue[700],
    "&:hover": {
      color: blue[900]
    }
  }
}));

const RegisterForm = ({ match, history }) => {
  const c = useStyles();
  const [name, setName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const auth = React.useContext(AuthContext);
  const state = React.useContext(LoginContext);
  const notifications = React.useContext(NotificationContext);

  const register = () => {
    auth
      .register(name, email, password)
      .then((res) => {
        console.log(res);
        if (!res.success) {
          notifications.sendNotification(res.message);
        } else {
          notifications.sendNotification("Account has been created");
          //history.push("/dashboard");
        }
      })
  };

  return (
    <div className={c.root}>
      <div className={c.topText}>
        Create a new account or{" "}
        <div className={c.loginText} onClick={() => state.switchForm()}>
          login
        </div>
      </div>
      <TextField
        label="Full Name"
        className={c.textField}
        value={name}
        onChange={ev => setName(ev.target.value)}
        margin="normal"
      />
      <TextField
        label="Email"
        className={c.textField}
        value={email}
        onChange={ev => setEmail(ev.target.value)}
        margin="normal"
      />
      <TextField
        label="Password"
        className={c.textField}
        value={password}
        onChange={ev => setPassword(ev.target.value)}
        margin="normal"
      />
      <div className={c.createButton}>
        <Button variant="contained" color="primary" onClick={() => register()}>
          Create
        </Button>
      </div>
    </div>
  );
};

export default observer(RegisterForm);

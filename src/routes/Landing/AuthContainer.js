import React from "react";
import { Paper } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
//import btn from "./btn_google_signin_dark_normal_web.png";
import logo from "./transparent.png";
import RegisterForm from "./RegisterForm";
import LoginForm from "./LoginForm";
import LoginContext from "../../contexts/LoginContext";

const useStyles = makeStyles(theme => ({
  root: {},
  container: {
    padding: theme.spacing.unit * 2,
    display: "flex"
  },
  panel: {
    display: "inline-block",
    verticalAlign: "middle"
  },
  divider: {
    margin: "0 1rem",
    width: "1px",
    backgroundColor: theme.palette.grey[500],
    display: "inline-block",
    flex: 1
  },
  auth: {
    height: "100%",
    display: "flex",
    alignItems: "center"
  },
  signIn: {
    display: "block",
    width: 191,
    height: 46,
    cursor: "pointer",
    marginRight: "0.25rem"
  },
  logo: {
    display: "flex",
    alignItems: "center",
    height: "100%"
  },
  logoImg: {
    maxHeight: "200px"
  }
}));

/*
google sign in button
<button
  className={c.signIn}
  style={{ background: `url(${btn})` }}
/>
*/
const AuthContainer = () => {
  const c = useStyles();
  const [showLogin, setShowLogin] = React.useState(false);

  return (
    <LoginContext.Provider
      value={{
        showLogin,
        switchForm: () => setShowLogin(!showLogin)
      }}
    >
      <div className={c.root}>
        <Paper className={c.container}>
          <div className={c.panel}>
            <div className={c.auth}>
              {showLogin ? <LoginForm /> : <RegisterForm />}
            </div>
          </div>
          <div className={c.divider} />
          <div className={c.panel}>
            <div className={c.logo}>
              <img className={c.logoImg} src={logo} alt="Logo" />
            </div>
          </div>
        </Paper>
      </div>
    </LoginContext.Provider>
  );
};

export default AuthContainer;

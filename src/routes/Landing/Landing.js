import React from "react";
import { makeStyles } from "@material-ui/styles";
import AuthContainer from "./AuthContainer";
import AuthContext from "../../contexts/AuthContext";
import { Redirect } from "react-router-dom";
import { observer } from "mobx-react-lite";
import AppSnackbar from "../../components/UI/Snackbar";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    height: "100%",
    position: "relative",
    background: theme.palette.primary.main
  },
  logo: {},
  authContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%"
  }
}));

const Landing = () => {
  const c = useStyles();
  const auth = React.useContext(AuthContext);

  return (
    <div className={c.root}>
      <div className={c.authContainer}>
        <AuthContainer />
        <AppSnackbar />
        {auth.authed && <Redirect to="/dashboard" />}
      </div>
    </div>
  );
};

export default observer(Landing);

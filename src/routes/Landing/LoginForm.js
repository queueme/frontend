import React from "react";
import { makeStyles } from "@material-ui/styles";
import { TextField, Button } from "@material-ui/core";
import LoginContext from "../../contexts/LoginContext";
import blue from "@material-ui/core/colors/blue";
import { AuthContext, NotificationContext } from "../../contexts";

const useStyles = makeStyles(theme => ({
  root: {},
  topText: {
    textAlign: "center"
  },
  textField: {
    display: "block !important"
  },
  loginButton: {
    marginTop: "1rem",
    display: "flex",
    justifyContent: "center"
  },
  loginText: {
    display: "inline-block",
    cursor: "pointer",
    color: blue[700],
    "&:hover": {
      color: blue[900]
    }
  }
}));

const LoginForm = () => {
  const c = useStyles();
  const [email, setEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const auth = React.useContext(AuthContext);
  const state = React.useContext(LoginContext);
  const notifications = React.useContext(NotificationContext);

  const login = () => {
    auth.login(email, password).then(res => {
      if (!res.success) {
        notifications.sendNotification(res.message);
      } else {
        notifications.sendNotification("Logged in successfully");
      }
    });
  };

  return (
    <div className={c.root}>
      <div className={c.topText}>
        Sign in with an existing account or{" "}
        <div className={c.loginText} onClick={() => state.switchForm()}>
          register
        </div>
      </div>
      <TextField
        label="Email"
        className={c.textField}
        value={email}
        onChange={ev => setEmail(ev.target.value)}
        margin="normal"
      />
      <TextField
        label="Password"
        className={c.textField}
        value={password}
        onChange={ev => setPassword(ev.target.value)}
        margin="normal"
        type="password"
      />
      <div className={c.loginButton}>
        <Button variant="contained" color="primary" onClick={() => login()}>
          Sign In
        </Button>
      </div>
    </div>
  );
};

export default LoginForm;

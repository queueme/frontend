import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import BusinessHome from "./BusinessHome/BusinessHome";
import BusinessVenues from "./BusinessVenues/BusinessVenues";

const Layout = ({ match }) => (
  <Switch>
    <Route exact path={`${match.url}`} component={BusinessHome} />
    <Route path={`${match.url}/venues`} component={BusinessVenues} />
    <Route component={() => <div>Not Found</div>} />
  </Switch>
);

export default withRouter(Layout);

import React from "react";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";
import SingleQueueContext from "./SingleQueueContext";
import { List, ListItem, ListItemText, Paper } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column"
  },
  title: {
    marginBottom: "0.5rem",
    ...theme.typography.h6
  },
  listRoot: {
    flex: 1,
    height: "100%",
    backgroundColor: theme.palette.background.paper
  },
  paper: {
    flex: 1
  }
}));

const Statistics = () => {
  const c = useStyles();
  const queue = React.useContext(SingleQueueContext);

  return (
    <div className={c.root}>
      <div className={c.title}>Visitors Queued</div>
      <Paper className={c.paper}>
        <List classes={{ root: c.listRoot }}>
          {queue.visitors.length === 0 ? (
            <ListItem>
              <ListItemText primary="No visitors currently queued" />
            </ListItem>
          ) : (
            queue.visitors.map(v => (
              <ListItem key={v.id}>
                <ListItemText
                  primary={v.fullName}
                  secondary="In line for 12 minutes"
                />
              </ListItem>
            ))
          )}
        </List>
      </Paper>
    </div>
  );
};

export default observer(Statistics);

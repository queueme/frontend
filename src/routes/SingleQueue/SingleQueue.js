import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { observer } from "mobx-react-lite";
import SingleVenueContext from "../SingleVenue/SingleVenueContext";
import SingleQueueContext from "./SingleQueueContext";
import ViewQueue from "./ViewQueue";

const SingleQueue = ({ match }) => {
  const venue = React.useContext(SingleVenueContext);

  const queue = venue.queues.find(p => p.id.toString() === match.params.id);

  if (!queue) {
    return <div>Not found.</div>;
  }

  return (
    <SingleQueueContext.Provider value={queue}>
      <Switch>
        <Route exact path={`${match.url}`} component={ViewQueue} />
      </Switch>
    </SingleQueueContext.Provider>
  );
};

export default withRouter(observer(SingleQueue));

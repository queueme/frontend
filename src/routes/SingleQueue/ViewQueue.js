import React from "react";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";
import { withRouter } from "react-router-dom";
import SingleQueueContext from "./SingleQueueContext";
import Statistics from "./Statistics";
import VisitorList from "./VisitorList";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import NotificationContext from "../../contexts/NotificationContext";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import SingleVenueContext from "../SingleVenue/SingleVenueContext";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    flex: 1
  },
  title: {
    ...theme.typography.h4,
    marginBottom: "1rem"
  },
  container: {
    display: "flex",
    flex: 1,
    justifyContent: "center"
  },
  spacer: {
    margin: "0 2rem"
  },
  fabDelete: {
    position: "absolute !important",
    top: "5rem",
    right: "2rem",
    flex: 0
  }
}));

const SingleQueue = ({ history, match }) => {
  const c = useStyles();
  const queue = React.useContext(SingleQueueContext);
  const [open, setOpen] = React.useState(false);
  const notes = React.useContext(NotificationContext);
  const venue = React.useContext(SingleVenueContext);

  const handleClose = () => setOpen(false);

  return (
    <div className={c.root}>
      <div className={c.title}>Viewing queue {queue.name}</div>
      <div className={c.container}>
        <Statistics />
        <div className={c.spacer} />
        <VisitorList />
      </div>
      <IconButton
        className={c.fabDelete}
        aria-label="Delete"
        onClick={() => setOpen(true)}
      >
        <DeleteIcon />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete this queue?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This queue will be permanently deleted.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() =>
              venue
                .deleteQueue(queue)
                .then(() => handleClose())
                .then(() =>
                  history.push(match.url.slice(0, match.url.lastIndexOf("/")))
                )
                .then(() => notes.sendNotification("Queue has been deleted"))
                .then(() => venue.removeQueue(queue))
            }
            color="primary"
          >
            Confirm
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default withRouter(observer(SingleQueue));

import React from "react";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";
import SingleQueueContext from "./SingleQueueContext";
import Tile from "../SingleVenue/Tile";
import { PeopleOutline, AccessTime, Autorenew } from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-evenly"
  }
}));

const Statistics = () => {
  const c = useStyles();
  const queue = React.useContext(SingleQueueContext);

  return (
    <div className={c.root}>
      <Tile
        property={queue.visitors.length}
        descriptor="Visitors queued"
        icon={PeopleOutline}
        colour="#3f51b5"
      />
      <Tile
        property={queue.avgWaitTime + " min"}
        descriptor="Average wait time"
        icon={AccessTime}
        colour="#4caf50"
      />
      <Tile
        property={queue.maxWaitTime + " min"}
        descriptor="Maximum wait time"
        icon={Autorenew}
        colour="#ff5722"
      />
    </div>
  );
};

export default observer(Statistics);

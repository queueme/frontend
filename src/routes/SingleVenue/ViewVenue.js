import React from "react";
import { makeStyles } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import { observer } from "mobx-react-lite";
import Statistics from "./Statistics";
import Queues from "./Queues";
import SingleVenueContext from "./SingleVenueContext";
import Fab from "@material-ui/core/Fab";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import DeleteIcon from "@material-ui/icons/Delete";
import VenueContext from "../../contexts/VenueContext";
import NotificationContext from "../../contexts/NotificationContext";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flex: 1,
    position: "relative",
    flexDirection: "column"
  },
  fab: {
    position: "absolute !important",
    bottom: "2rem",
    right: "2rem",
    flex: 0
  },
  title: {
    ...theme.typography.h4,
    marginBottom: "1rem"
  },
  fabDelete: {
    position: "absolute !important",
    top: "5rem",
    right: "2rem",
    flex: 0
  }
}));

const SingleVenue = ({ match, history }) => {
  const c = useStyles();
  const v = React.useContext(SingleVenueContext);
  const store = React.useContext(VenueContext);
  const [open, setOpen] = React.useState(false);
  const notes = React.useContext(NotificationContext);

  const handleClose = () => setOpen(false);

  return (
    <SingleVenueContext.Provider value={v}>
      <div className={c.root}>
        <div className={c.title}>Viewing venue {v.name}</div>
        <Statistics />
        <Queues />
      </div>
      <Fab
        color="secondary"
        aria-label="add"
        className={c.fab}
        onClick={() => history.push(`${match.url}/create`)}
      >
        <AddIcon />
      </Fab>
      <IconButton
        className={c.fabDelete}
        aria-label="Delete"
        onClick={() => setOpen(true)}
      >
        <DeleteIcon />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Delete this venue?</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            This venue will be permanently deleted.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() =>
              store
                .deleteVenue(v)
                .then(() => handleClose())
                .then(() =>
                  history.push(match.url.slice(0, match.url.lastIndexOf("/")))
                )
                .then(() => notes.sendNotification("Venue has been deleted"))
                .then(() => store.removeVenue(v))
                
            }
            color="primary"
          >
            Confirm
          </Button>
          <Button onClick={handleClose} color="primary" autoFocus>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </SingleVenueContext.Provider>
  );
};

export default withRouter(observer(SingleVenue));

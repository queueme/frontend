import React from "react";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";
import SingleVenueContext from "./SingleVenueContext";
import { Paper, Chip, Button } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { useState } from 'react';

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flex: 1,
    flexDirection: "column"
  },
  title: {
    ...theme.typography.title,
    marginTop: "2rem",
    width: "100%"
  },
  container: {
    marginTop: "1rem",
    display: "flex"
  },
  qContainer: {
    padding: "1.5rem",
    marginRight: "1rem",
    maxWidth: "250px"
  },
  qTitle: {
    ...theme.typography.h5,
    flex: 1,
    marginRight: "1rem"
  },
  header: {
    display: "flex"
  },
  chip: {},
  chipBusy: {
    borderColor: theme.palette.error[500] + " !important",
    color: theme.palette.error[500] + " !important"
  },
  stats: {
    margin: "1.5rem 0"
  },
  statsCon: {
    marginBottom: "0.5rem"
  },
  statsKey: {
    display: "inline-block",
    marginRight: "0.25rem"
  },
  statsVal: {
    display: "inline-block"
  },
  actions: {
    display: "flex",
    justifyContent: "space-around"
  },
  button: {
    backgroundColor: theme.palette.secondary + " !important"
  },
  backdrop:{
    backgroundColor: " "
  }
}));
 
const QueueTile = withRouter(({ name, history, match, id }) => {
  const [color, setColor] = useState(false);
  const c = useStyles();

  return (
    <Paper className={c.qContainer + " " + (color ? c.button: c.backdrop)}>
      <div className={c.header}>
        <div className={c.qTitle}>{name}</div>
        <div className={c.chip}>
          <Chip
            label="Busy"
            className={c.chip + " " + c.chipBusy}
            variant="outlined"
          />
        </div>
      </div>
      <div className={c.stats}>
        <div className={c.statsCon}>
          <div className={c.statsKey}>In Queue:</div>
          <div className={c.statsVal}>25</div>
        </div>
        <div className={c.statsCon}>
          <div className={c.statsKey}>Visited Today:</div>
          <div className={c.statsVal}>193</div>
        </div>
        <div className={c.statsCon}>
          <div className={c.statsKey}>Capacity:</div>
          <div className={c.statsVal}>35</div>
        </div>
      </div>
      <div className={c.actions}>
        <Button
          variant="contained"
          color="secondary"
          className={c.button}
          style={{ marginRight: "0.5rem" }}
          onClick={() => history.push(`${match.url}/${id}`)}
        >
          View
        </Button>
        <Button variant="contained" onClick={() => setColor(true)}>
          Deactivate
        </Button>
      </div>
    </Paper>
  );
});

const Queues = ({ name }) => {
  const c = useStyles();
  const { queues } = React.useContext(SingleVenueContext);

  if (queues.length === 0) {
    return (
      <div className={c.root}>
        <div className={c.title} style={{ textAlign: "center" }}>
          You do not have any queues yet. Use the floating action button to
          create one.
        </div>
      </div>
    );
  }
  return (
    <div className={c.root}>
      <div className={c.title}>Queues at this venue</div>
      <div className={c.container}>
        {queues.map(q => (
          <QueueTile {...q} key={q.id}  className={c.backdrop}/>
        ))}
      </div>
    </div>
  );
};

export default observer(Queues)

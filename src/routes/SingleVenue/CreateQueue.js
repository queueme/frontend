import React from "react";
import { makeStyles } from "@material-ui/styles";
import { TextField, Button } from "@material-ui/core";
import SingleVenueContext from "./SingleVenueContext";
import { withRouter } from "react-router-dom";
import NotificationContext from "../../contexts/NotificationContext";

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: theme.breakpoints.width("md"),
    width: "100%",
    margin: "0 auto"
  },
  title: {
    ...theme.typography.h4,
    marginBottom: theme.spacing.unit * 2
  },
  textField: { display: "block !important" },
  container: {},
  form: {},
  submit: {
    marginTop: theme.spacing.unit * 2 + "px !important"
  }
}));

const CreateQueue = ({ match, history }) => {
  const c = useStyles();
  const venue = React.useContext(SingleVenueContext);
  const [name, setName] = React.useState("");
  const [desc, setDesc] = React.useState("");
  const notifications = React.useContext(NotificationContext);

  const submit = () => {
    venue.createQueue(name, desc).then(res => {
      if (!res.success) {
        notifications.sendNotification(res.message);
      } else {
        notifications.sendNotification("New queue has been created");
        history.push(match.url.slice(0, match.url.lastIndexOf("/")));
      }
    });
  };

  return (
    <div className={c.root}>
      <div className={c.title}>Create a new queue for venue {venue.name}</div>
      <div className={c.container}>
        <TextField
          label="Name"
          className={c.textField}
          value={name}
          onChange={ev => setName(ev.target.value)}
          margin="normal"
          required
        />
        <TextField
          label="Description"
          className={c.textField}
          value={desc}
          onChange={ev => setDesc(ev.target.value)}
          margin="normal"
          required
        />
        <Button
          variant="contained"
          color="primary"
          className={c.submit}
          onClick={() => submit()}
        >
          Create Queue
        </Button>
      </div>
    </div>
  );
};

export default withRouter(CreateQueue);

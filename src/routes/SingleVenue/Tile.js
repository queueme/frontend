import React from "react";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flex: 1,
    maxHeight: "200px",
    maxWidth: "400px",
    color: "white",
    boxSizing: "border-box",
    padding: "1rem",
    margin: "1rem",
    alignItems: "center"
  },
  property: {
    ...theme.typography.h2,
    color: "white"
  },
  descriptor: {
    ...theme.typography.h6,
    textTransform: "uppercase",
    color: "white"
  },
  left: { display: "inline-block", verticalAlign: "middle", flex: 1 },
  right: { display: "inline-block", verticalAlign: "bottom" },
  icon: {
    fontSize: 96 + "px !important"
  }
}));

const Tile = ({ property, descriptor, icon: Icon, colour }) => {
  const c = useStyles();

  return (
    <div className={c.root} style={{ backgroundColor: colour }}>
      <div className={c.left}>
        <div className={c.property}>{property}</div>
        <div className={c.descriptor}>{descriptor}</div>
      </div>
      <div className={c.right}>
        <Icon className={c.icon} />
      </div>
    </div>
  );
};

export default observer(Tile);

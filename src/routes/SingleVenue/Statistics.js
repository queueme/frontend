import React from "react";
import { makeStyles } from "@material-ui/styles";
import { observer } from "mobx-react-lite";
import SingleVenueContext from "./SingleVenueContext";
import Tile from "./Tile";
import { PeopleOutline, AccessTime, Autorenew } from "@material-ui/icons";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    justifyContent: "space-evenly"
  }
}));

const Statistics = () => {
  const c = useStyles();
  const venue = React.useContext(SingleVenueContext);

  return (
    <div className={c.root}>
      <Tile
        property={venue.visitors}
        descriptor="Visitors at this venue"
        icon={PeopleOutline}
        colour="#3f51b5"
      />
      <Tile
        property={venue.avgWaitTime + " min"}
        descriptor="Average wait time"
        icon={AccessTime}
        colour="#4caf50"
      />
      <Tile
        property={venue.maxWaitTime + " min"}
        descriptor="Maximum wait time"
        icon={Autorenew}
        colour="#ff5722"
      />
    </div>
  );
};

export default observer(Statistics);

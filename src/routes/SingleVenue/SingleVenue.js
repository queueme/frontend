import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import { observer } from "mobx-react-lite";
import SingleVenueContext from "./SingleVenueContext";
import VenueContext from "../../contexts/VenueContext";
import CreateQueue from "./CreateQueue";
import ViewVenue from "./ViewVenue";
import SingleQueue from"../SingleQueue/SingleQueue";

const SingleVenue = ({ match }) => {
  const venues = React.useContext(VenueContext);

  const v = venues.list.find(p => p.id.toString() === match.params.id);

  if (!v) {
    return <div>Not found.</div>;
  }

  return (
    <SingleVenueContext.Provider value={v}>
      <Switch>
        <Route exact path={`${match.url}`} component={ViewVenue} />
        <Route exact path={`${match.url}/create`} component={CreateQueue} />
        <Route path={`${match.url}/:id`} component={SingleQueue} />
      </Switch>
    </SingleVenueContext.Provider>
  );
};

export default withRouter(observer(SingleVenue));

import React from "react";
import {
  Drawer,
  Hidden,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { makeStyles } from "@material-ui/styles";
import HomeIcon from "@material-ui/icons/Home";
import BusinessIcon from "@material-ui/icons/Business";

const useStyles = makeStyles(theme => ({
  root: {
    flex: 0,
    boxShadow: theme.shadows[8],
    zIndex: 5
  },
  hidden: {
    height: "100%"
  },
  drawerDocked: {
    height: "100%"
  },
  drawerSpacer: {
    minHeight: "64px"
  },
  drawerModal: {},
  drawerPaper: {
    display: "flex",
    width: 250,
    borderRight: "none",
    [theme.breakpoints.up("md")]: {
      width: 240,
      position: "relative",
      height: "100%"
    }
  },
  listText: {
    color: theme.palette.common.white
  },
  listIcon: {
    color: theme.palette.common.white,
    marginRight: 0
  },
  selected: {
    backgroundColor: `${theme.palette.grey[300]} !important`
  }
}));

const items = [
  { path: "/dashboard", name: "Dashboard", icon: <HomeIcon /> },
  { path: "/dashboard/venues", name: "Venues", icon: <BusinessIcon /> }
];

const SidebarItems = withRouter(({ location, history }) => {
  const c = useStyles({});
  return (
    <List>
      {items.map(item => (
        <ListItem
          button
          key={item.name}
          onClick={() => history.push(item.path)}
          classes={
            location.pathname === item.path ? { root: c.selected } : undefined
          }
        >
          <ListItemIcon classes={{ root: c.listIcon }}>
            {item.icon}
          </ListItemIcon>
          <ListItemText primary={item.name} classes={{ primary: c.listText }} />
        </ListItem>
      ))}
    </List>
  );
});

const Sidebar = ({ open, setOpen }) => {
  const c = useStyles({});
  return (
    <div className={c.root}>
      <Hidden mdUp>
        <Drawer
          variant="temporary"
          anchor="left"
          open={open}
          classes={{
            modal: c.drawerModal,
            paper: c.drawerPaper
          }}
          onClose={() => setOpen(false)}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          <SidebarItems />
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css" className={c.hidden}>
        <Drawer
          variant="permanent"
          open
          classes={{
            docked: c.drawerDocked,
            paper: c.drawerPaper
          }}
        >
          <div className={c.drawerSpacer} />
          <SidebarItems />
        </Drawer>
      </Hidden>
    </div>
  );
};

export default Sidebar;

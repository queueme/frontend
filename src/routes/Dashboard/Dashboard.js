import React from "react";
import { makeStyles } from "@material-ui/styles";
import {
  AppBar,
  Toolbar,
  IconButton,
  Badge,
  Typography,
  Avatar,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import NotificationsIcon from "@material-ui/icons/Notifications";
import Sidebar from "./Sidebar";
import MenuIcon from "@material-ui/icons/Menu";
import DashboardLayout from "../DashboardLayout";
import AppSnackbar from "../../components/UI/Snackbar";
import { AuthContext, NotificationContext } from "../../contexts";
import LogoutIcon from "@material-ui/icons/ExitToApp";
import { observer } from "mobx-react-lite";
import { Redirect } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    height: "100%",
    flex: 1,
    display: "flex",
    position: "relative",
    flexDirection: "column"
  },
  bar: { zIndex: theme.zIndex.drawer + 1 },
  headerSpacer: {
    flexGrow: 1
  },
  leftSection: {
    display: "flex",
    alignItems: "center"
  },
  rightSection: {},
  mobileToggle: {
    marginRight: "1.25rem",
    [theme.breakpoints.up("md")]: {
      display: "none"
    }
  },
  iconColor: {
    color: "white !important"
  },
  container: {
    flex: 1,
    display: "flex",
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: `calc(100% - 241px)`,
      marginLeft: "241px"
    },
    padding: theme.spacing.unit * 2,
    boxSizing: "border-box",
    overflowY: "auto",
    overflowX: "auto"
  },
  avatar: {
    backgroundColor: theme.palette.grey[400],
    cursor: "pointer"
  },
  avatarWrapper: {
    display: "inline-block",
    verticalAlign: "middle"
  }
}));

const Dashboard = () => {
  const c = useStyles();
  const [open, setOpen] = React.useState(true);
  const [dropOpen, setDropOpen] = React.useState({ open: false, anchor: null });
  const auth = React.useContext(AuthContext);
  const store = React.useContext(NotificationContext);

  return (
    <div className={c.root}>
      <Sidebar open={open} setOpen={setOpen} />
      <AppBar position="static" className={c.bar}>
        <Toolbar>
          <div className={c.leftSection}>
            <Typography variant="h6" color="inherit" noWrap>
              QueueMe
            </Typography>
            <div className={c.mobileToggle}>
              <IconButton
                color="primary"
                classes={{ root: c.iconColor }}
                aria-label="open drawer"
                onClick={() => setOpen(!open)}
              >
                <MenuIcon />
              </IconButton>
            </div>
          </div>
          <div className={c.headerSpacer} />
          <div className={c.rightSection}>
            <IconButton color="inherit">
              <Badge badgeContent={0} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
            <div className={c.avatarWrapper}>
              <Avatar
                className={c.avatar}
                aria-label="User options"
                aria-owns={"avatar-menu"}
                aria-haspopup="true"
                onClick={ev =>
                  setDropOpen({ open: true, anchor: ev.currentTarget })
                }
              >
                {auth && auth.initials}
              </Avatar>
            </div>
            <Menu
              id="avatar-menu"
              open={dropOpen.open}
              anchorEl={dropOpen.anchor}
              onClose={() =>
                setDropOpen({ open: false, anchor: dropOpen.anchor })
              }
            >
              <MenuItem
                onClick={() => {
                  setDropOpen({ open: false, anchor: dropOpen.anchor });
                  auth.logout();
                  store.sendNotification("Logged out successfully");
                }}
              >
                <ListItemIcon>
                  <LogoutIcon />
                </ListItemIcon>
                <ListItemText inset primary="Logout" />
              </MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
      <div className={c.container}>
        <DashboardLayout />
      </div>
      <AppSnackbar />
      {!auth.authed && <Redirect to="/" />}
    </div>
  );
};

export default observer(Dashboard);

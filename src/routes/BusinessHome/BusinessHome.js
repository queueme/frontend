import React from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(theme => ({
  root: {
    flex: 1,
    width: "100%",
    margin: "0 auto"
  }
}));

const Map = withScriptjs(
  withGoogleMap(props => (
    <GoogleMap defaultZoom={16} defaultCenter={{ lat: 49.260920, lng: -123.248801 }}>
      <Marker position={{ lat: 49.260920, lng: -123.248801 }} />
    </GoogleMap>
  ))
);

export default () => {
  const c = useStyles();
  return (
    <div className={c.root}>
      <Map
        googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAmVXdZvEKBpxXzsPdgzVrH5YuMqGKI3gA&v=3.exp&libraries=geometry,drawing,places"
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `100%` }} />}
        mapElement={<div style={{ height: `100%` }} />}
      />
    </div>
  );
};

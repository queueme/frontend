import React from "react";
import { makeStyles } from "@material-ui/styles";
import venue1Image from "./clinic.jpg";
import VenueCard from "./VenueCard";
import { VenueContext } from "../../contexts";
import { observer } from "mobx-react-lite";
import { Animated } from "react-animated-css";

const images = [venue1Image];

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexFlow: "wrap",
    overflow: "auto"
  },
  container: {
    display: "grid",
    gridTemplateColumns: "1fr 1fr 1fr",
    backgroundColor: theme.palette.grey[300]
  },
  loading: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "100%"
  },
  card: {
    margin: "1rem"
  },
  cards: {
    float: "left",
    paddingLeft: "60px"
  },
  title: {
    ...theme.typography.title,
    marginTop: "2rem",
    width: "100%"
  }
  // cards: {
  //   backgroundColor: "yellow",
  //   '&:hover': {
  //     backgroundColor: "blue"
  //   }
  // }
}));

const VenueContainer = () => {
  const c = useStyles();
  const venues = React.useContext(VenueContext);

  if (venues.list.length === 0) {
    return (
      <div className={c.root}>
        <div className={c.title} style={{ textAlign: "center" }}>
          You do not have any venues yet. Use the floating action button to
          create one.
        </div>
      </div>
    );
  }

  return (
    <div className={c.root}>
      {venues.list.map(function(venue, i) {
        return (
          <div className={c.card} key={venue.id}>
            <Animated
              animationIn="fadeInLeft"
              animationOut="fadeOut"
              isVisible={true}
              className={c.cards}
            >
              <VenueCard
                venueId={venue.id}
                venueName={venue.name}
                venueImage={images[i % images.length]}
                venueDescription={venue.description}
                venueAddress={venue.address || ""}
              />
            </Animated>
          </div>
        );
      })}
    </div>
  );
};

/**
 * The venues page, calls venueCard for every venue added to the page
 * Can add as many venues as needed
 */

export default observer(VenueContainer);

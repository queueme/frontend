import React from "react";
import { makeStyles } from "@material-ui/styles";
import Fab from "@material-ui/core/Fab";
import AddIcon from "@material-ui/icons/Add";
import VenueContainer from "./VenueContainer";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flex: 1,
    position: "relative"
  },
  fab: {
    position: "absolute !important",
    bottom: "1rem",
    right: "1rem"
  }
}));

const BusinessVenues = ({ match, history }) => {
  const c = useStyles();

  return (
    <div className={c.root}>
      <VenueContainer />
      <Fab
        color="secondary"
        aria-label="add"
        className={c.fab}
        onClick={() => history.push(`${match.url}/create`)}
      >
        <AddIcon />
      </Fab>
    </div>
  );
};

export default BusinessVenues;

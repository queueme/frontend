import React from "react";
import { makeStyles } from "@material-ui/styles";
import { TextField, Button } from "@material-ui/core";
import VenueContext from "../../contexts/VenueContext";
import { withRouter } from "react-router-dom";
import NotificationContext from "../../contexts/NotificationContext";

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: theme.breakpoints.width("md"),
    width: "100%",
    margin: "0 auto"
  },
  title: {
    ...theme.typography.h4,
    marginBottom: theme.spacing.unit * 2
  },
  textField: { display: "block !important" },
  container: {},
  form: {},
  submit: {
    marginTop: theme.spacing.unit * 2 + "px !important"
  }
}));

const CreateVenue = ({ match, history }) => {
  const c = useStyles();
  const [name, setName] = React.useState("");
  const [desc, setDesc] = React.useState("");
  const [addr, setAddr] = React.useState("");
  const venues = React.useContext(VenueContext);
  const notifications = React.useContext(NotificationContext);

  const submit = () => {
    venues.createVenue(name, desc, addr).then(res => {
      if (!res.success) {
        notifications.sendNotification(res.message);
      } else {
        notifications.sendNotification("New venue has been created");
        history.push(match.url.slice(0, match.url.lastIndexOf("/")));
      }
    });
  };

  return (
    <div className={c.root}>
      <div className={c.title}>Create a venue</div>
      <div className={c.container}>
        <TextField
          label="Name"
          className={c.textField}
          value={name}
          onChange={ev => setName(ev.target.value)}
          margin="normal"
          required
        />
        <TextField
          label="Description"
          className={c.textField}
          value={desc}
          onChange={ev => setDesc(ev.target.value)}
          margin="normal"
          required
        />
        <TextField
          label="Address"
          className={c.textField}
          value={addr}
          onChange={ev => setAddr(ev.target.value)}
          margin="normal"
        />
        <Button
          variant="contained"
          color="primary"
          className={c.submit}
          onClick={() => submit()}
        >
          Create Venue
        </Button>
      </div>
    </div>
  );
};

export default withRouter(CreateVenue);

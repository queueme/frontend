import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import ViewVenues from "./ViewVenues";
import CreateVenue from "./CreateVenue";
import { VenueContext } from "../../contexts";
import { observer } from "mobx-react-lite";
import SingleVenue from "../SingleVenue/SingleVenue";
import LoadingPage from "../../components/UI/LoadingPage";

const Layout = ({ match }) => {
  const venues = React.useContext(VenueContext);

  React.useEffect(() => {
    venues.loadVenues();
  }, []);

  if (venues.loading) {
    return <LoadingPage />;
  }

  return (
    <Switch>
      <Route exact path={`${match.url}`} component={ViewVenues} />
      <Route exact path={`${match.url}/create`} component={CreateVenue} />
      <Route path={`${match.url}/:id`} component={SingleVenue} />
    </Switch>
  );
};

export default withRouter(observer(Layout));

import React from "react";
import { makeStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import { withRouter } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  card: {
    width: 350,
    height: 410
  },
  image: {
    height: "50%"
  },
  cardTitle: {
    cursor: "pointer",
    "&:hover": {
      color: theme.palette.grey[600]
    }
  }
}));

function VenueCard({
  venueId,
  venueName,
  venueImage,
  venueDescription,
  venueAddress,
  match,
  history
}) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <CardHeader
        avatar={
          <Avatar aria-label="" className={classes.avatar}>
            {venueName.slice(0, 1).toUpperCase()}
          </Avatar>
        }
        title={
          <Typography
            variant="h5"
            classes={{ root: classes.cardTitle }}
            onClick={() => history.push(`${match.url}/${venueId}`)}
          >
            {venueName}
          </Typography>
        }
        subheader=""
      />
      <CardMedia
        className={classes.image}
        image={venueImage}
        title="Paella dish"
      />
      <CardContent>
        <Typography variant="overline">Description</Typography>
        <Typography>{venueDescription}</Typography>
        {venueAddress && <Typography variant="overline">Address</Typography>}
        {venueAddress && <Typography>{venueAddress}</Typography>}
      </CardContent>
    </Card>
  );
}

export default withRouter(VenueCard);
